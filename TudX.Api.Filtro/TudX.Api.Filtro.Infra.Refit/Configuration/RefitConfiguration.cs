﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Filtro.Infra.Refit.Configuration
{
    public class RefitConfiguration
    {
        public string UrlBaseApiFiltro { get; set; }
    }
}
