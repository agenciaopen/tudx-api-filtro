﻿using Refit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TudX.Api.Filtro.Infra.Refit.Clients
{
    public interface IApiFiltro
    {
        [Get("/healthz")]
        Task<string> HealthChecksAsync();
    }
}
