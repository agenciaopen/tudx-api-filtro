﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Filtro.Infra.EF.Mappings
{
    public static class FiltroMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Domain.Model.Filtro>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.Nome).IsRequired();
                ent.Property(a => a.Ativo).IsRequired();
                ent.Property(a => a.Modificado).IsRequired();
                ent.Property(a => a.IdentificadorCategoria).IsRequired();
                ent.Property(a => a.IdentificadorCaracteristica);
                ent.Property(a => a.Tutorial);

                ent.HasMany(x => x.Opcoes)
                   .WithOne(x => x.Filtro);
            });
        }
    }
}
