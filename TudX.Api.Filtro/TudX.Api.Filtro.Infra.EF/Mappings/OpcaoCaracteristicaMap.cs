﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Filtro.Domain.Model;

namespace TudX.Api.Filtro.Infra.EF.Mappings
{
    public static class OpcaoCaracteristicaMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OpcaoCaracteristica>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.Valor).IsRequired();

                ent.HasOne(x => x.Caracteristica)
                .WithMany()
                .HasForeignKey("IdentificadorCaracteristica");
            });
        }
    }
}
