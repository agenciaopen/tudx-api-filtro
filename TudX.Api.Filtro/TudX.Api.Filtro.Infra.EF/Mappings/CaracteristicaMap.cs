﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Filtro.Domain.Model;

namespace TudX.Api.Filtro.Infra.EF.Mappings
{
    public static class CaracteristicaMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Caracteristica>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador).ValueGeneratedNever();
                ent.Property(a => a.Nome).IsRequired();
                ent.Property(a => a.IdentificadorSuperior);
                ent.Property(a => a.IdentificadorCategoria).IsRequired();

                ent.HasMany(e => e.CaracteristicasFilhas)
                   .WithOne(e => e.CaracteristicaSuperior)
                   .HasForeignKey(e => e.IdentificadorSuperior);

                ent.HasMany(x => x.Opcoes)
                    .WithOne(x => x.Caracteristica);
            });
        }
    }
}
