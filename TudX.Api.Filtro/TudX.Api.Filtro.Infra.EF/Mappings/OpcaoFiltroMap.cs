﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Filtro.Domain.Model;

namespace TudX.Api.Filtro.Infra.EF.Mappings
{
    public static class OpcaoFiltroMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OpcaoFiltro>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.Valor).IsRequired();
                ent.Property(a => a.TipoElemento).IsRequired();
                ent.Property(a => a.IdentificadorInformacaoProduto);

                ent.HasOne(x => x.Filtro)
                .WithMany()
                .HasForeignKey("IdentificadorFiltro");                

                ent.HasMany(x => x.Valores)
                .WithOne(x => x.OpcaoFiltro);
            });
        }
    }
}
