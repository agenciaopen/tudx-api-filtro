﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Filtro.Domain.Model;

namespace TudX.Api.Filtro.Infra.EF.Mappings
{
    public static class CategoriaMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Categoria>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador).ValueGeneratedNever();
                ent.Property(a => a.Nome).IsRequired();
                ent.Property(a => a.IdentificadorSuperior);

                ent.HasMany(e => e.CategoriasFilhas)
                   .WithOne(e => e.CategoriaSuperior)
                   .HasForeignKey(e => e.IdentificadorSuperior);
            });
        }
    }
}
