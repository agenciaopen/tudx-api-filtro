﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Filtro.Domain.Model;

namespace TudX.Api.Filtro.Infra.EF.Mappings
{
    public static class InformacaoProdutoMap
    {
        public static void Mapper(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InformacaoProduto>(ent =>
            {
                ent.HasKey(a => a.Identificador);
                ent.Property(a => a.Identificador);
                ent.Property(a => a.NomeCampo).IsRequired();
                ent.Property(a => a.TipoCampo).IsRequired();
            });
        }
    }
}
