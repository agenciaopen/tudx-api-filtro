﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TudX.Adapter.EntityFramework.Context;
using TudX.Api.Filtro.Domain.Model;
using TudX.Api.Filtro.Infra.EF.Mappings;

namespace TudX.Api.Filtro.Infra.EF.Context
{
    public class ApiFiltroDbContext : AppDataContext
    {
        public ApiFiltroDbContext(DbContextOptions<ApiFiltroDbContext> options) : base(options)
        {            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ForSqlServerUseIdentityColumns();

            InformacaoProdutoMap.Mapper(modelBuilder);
            OpcaoCaracteristicaMap.Mapper(modelBuilder);
            ValorFiltroMap.Mapper(modelBuilder);
            OpcaoFiltroMap.Mapper(modelBuilder);            
            FiltroMap.Mapper(modelBuilder);
            CaracteristicaMap.Mapper(modelBuilder);
            CategoriaMap.Mapper(modelBuilder);
            EventoEnviadoMap.Mapper(modelBuilder);
            EventoRecebidoMap.Mapper(modelBuilder);
        }

        public DbSet<Caracteristica> Caracteristica { get; set; }
        public DbSet<Categoria> Categoria { get; set; }
        public DbSet<Domain.Model.Filtro> Filtro { get; set; }
        public DbSet<InformacaoProduto> InformacaoProduto { get; set; }
        public DbSet<OpcaoCaracteristica> OpcaoCaracteristica { get; set; }
        public DbSet<OpcaoFiltro> OpcaoFiltro { get; set; }
        public DbSet<ValorFiltro> ValorFiltro { get; set; }
    }
}
