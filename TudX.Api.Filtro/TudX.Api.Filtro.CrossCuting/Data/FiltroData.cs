﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Filtro.CrossCuting.Data
{
    public class FiltroData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Tutorial { get; set; }        
        public long? IdentificadorCaracteristica { get; set; }
        public List<OpcaoFiltroData> Opcoes { get; set; }
    }
}
