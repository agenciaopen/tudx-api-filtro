﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Filtro.CrossCuting.Data
{
    public class CategoriaIntegrationEventData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public long? IdentificadorSuperior { get; set; }
    }
}
