﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Filtro.CrossCuting.Data
{
    public class InformacaoCaracteristicaProdutoIntegrationEventData
    {
        public long IdentificadorCategoria { get; set; }
        public List<CaracteristicaCategorizadoDataIntegrationEventData> CaracteristicasComValor { get; set; }
    }
}
