﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Filtro.CrossCuting.Data
{
    public class CaracteristicaNaoCategorizadaIntegrationEventData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public long IdentificadorCategoria { get; set; }
    }
}
