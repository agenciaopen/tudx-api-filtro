﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Filtro.CrossCuting.Data
{
    public class CaracteristicaCategorizadoDataIntegrationEventData
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Valor { get; set; }
        public long IdentificadorCaracteristica { get; set; }
    }
}
