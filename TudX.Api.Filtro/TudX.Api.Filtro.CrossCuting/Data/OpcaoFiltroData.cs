﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Filtro.Domain.Model.Enum;

namespace TudX.Api.Filtro.CrossCuting.Data
{
    public class OpcaoFiltroData
    {
        public long Identificador { get; set; }
        public string Valor { get; set; }
        public TipoElemento TipoElemento { get; set; }
    }
}
