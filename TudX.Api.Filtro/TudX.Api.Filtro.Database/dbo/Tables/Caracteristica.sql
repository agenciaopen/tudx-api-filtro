﻿CREATE TABLE [dbo].[Caracteristica] (
    [Identificador]          BIGINT       NOT NULL,
    [Nome]                   VARCHAR (50) NOT NULL,
    [IdentificadorCategoria] BIGINT       NOT NULL,
    [IdentificadorSuperior]  BIGINT       NULL,
    CONSTRAINT [PK_Caracteristica] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_Caracteristica_Caracteristica] FOREIGN KEY ([IdentificadorSuperior]) REFERENCES [dbo].[Caracteristica] ([Identificador])
);

