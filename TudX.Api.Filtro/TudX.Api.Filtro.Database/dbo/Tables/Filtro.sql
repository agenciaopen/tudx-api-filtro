﻿CREATE TABLE [dbo].[Filtro] (
    [Identificador]               BIGINT        IDENTITY (1, 1) NOT NULL,
    [Nome]                        VARCHAR (255) NOT NULL,
    [Tutorial]                    TEXT          NULL,
    [Ativo]                       BIT           NOT NULL,
    [Modificado]                  BIT           NOT NULL,
    [IdentificadorCategoria]      BIGINT        NOT NULL,
    [IdentificadorCaracteristica] BIGINT        NULL,
    CONSTRAINT [PK_Filtro] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_Filtro_Caracteristica] FOREIGN KEY ([IdentificadorCaracteristica]) REFERENCES [dbo].[Caracteristica] ([Identificador]),
    CONSTRAINT [FK_Filtro_Categoria] FOREIGN KEY ([IdentificadorCategoria]) REFERENCES [dbo].[Categoria] ([Identificador])
);

