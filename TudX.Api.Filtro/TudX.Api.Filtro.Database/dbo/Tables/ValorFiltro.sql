﻿CREATE TABLE [dbo].[ValorFiltro] (
    [Identificador]               BIGINT        IDENTITY (1, 1) NOT NULL,
    [Valor]                       VARCHAR (255) NOT NULL,
    [IdentificadorOpcaoFiltro]    BIGINT        NOT NULL,
    [IdentificadorCaracteristica] BIGINT        NOT NULL,
    CONSTRAINT [PK_ValorFiltro] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_ValorFiltro_Caracteristica] FOREIGN KEY ([IdentificadorCaracteristica]) REFERENCES [dbo].[Caracteristica] ([Identificador]),
    CONSTRAINT [FK_ValorFiltro_OpcaoFiltro] FOREIGN KEY ([IdentificadorOpcaoFiltro]) REFERENCES [dbo].[OpcaoFiltro] ([Identificador])
);

