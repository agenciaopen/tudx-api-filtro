﻿CREATE TABLE [dbo].[Categoria] (
    [Identificador]         BIGINT        NOT NULL,
    [Nome]                  VARCHAR (255) NOT NULL,
    [IdentificadorSuperior] BIGINT        NULL,
    CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_Categoria_Categoria] FOREIGN KEY ([IdentificadorSuperior]) REFERENCES [dbo].[Categoria] ([Identificador])
);

