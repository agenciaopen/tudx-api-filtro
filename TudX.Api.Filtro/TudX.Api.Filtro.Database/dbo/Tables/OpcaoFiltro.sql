﻿CREATE TABLE [dbo].[OpcaoFiltro] (
    [Identificador]                  BIGINT        IDENTITY (1, 1) NOT NULL,
    [Valor]                          VARCHAR (255) NOT NULL,
    [TipoElemento]                   SMALLINT      NOT NULL,
    [IdentificadorFiltro]            BIGINT        NOT NULL,
    [IdentificadorInformacaoProduto] BIGINT        NULL,
    CONSTRAINT [PK_OpcaoFiltro] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_OpcaoFiltro_Filtro] FOREIGN KEY ([IdentificadorFiltro]) REFERENCES [dbo].[Filtro] ([Identificador]),
    CONSTRAINT [FK_OpcaoFiltro_InformacaoProduto] FOREIGN KEY ([IdentificadorInformacaoProduto]) REFERENCES [dbo].[InformacaoProduto] ([Identificador])
);

