﻿CREATE TABLE [dbo].[OpcaoCaracteristica] (
    [Identificador]               BIGINT        IDENTITY (1, 1) NOT NULL,
    [Valor]                       VARCHAR (255) NOT NULL,
    [IdentificadorCaracteristica] BIGINT        NOT NULL,
    CONSTRAINT [PK_OpcaoCaracteristica] PRIMARY KEY CLUSTERED ([Identificador] ASC),
    CONSTRAINT [FK_OpcaoCaracteristica_Caracteristica] FOREIGN KEY ([IdentificadorCaracteristica]) REFERENCES [dbo].[Caracteristica] ([Identificador])
);

