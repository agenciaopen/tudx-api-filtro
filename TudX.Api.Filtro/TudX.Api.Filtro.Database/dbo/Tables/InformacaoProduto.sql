﻿CREATE TABLE [dbo].[InformacaoProduto] (
    [Identificador] BIGINT        IDENTITY (1, 1) NOT NULL,
    [NomeCampo]     VARCHAR (255) NOT NULL,
    [TipoCampo]     SMALLINT      NOT NULL,
    CONSTRAINT [PK_InformacaoProduto] PRIMARY KEY CLUSTERED ([Identificador] ASC)
);

