﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Filtro.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Filtro.Application.Specification
{
    public class FiltroPorIdentificadorDeCaracteristicaSpecification : SpecificationBase<Domain.Model.Filtro>
    {
        private long identificadorCaracteristica;

        public FiltroPorIdentificadorDeCaracteristicaSpecification(long identificadorCaracteristica)
        {
            this.identificadorCaracteristica = identificadorCaracteristica;
        }

        public override Expression<Func<Domain.Model.Filtro, bool>> SatisfiedBy()
        {
            return f => f.IdentificadorCaracteristica == identificadorCaracteristica;
        }
    }
}
