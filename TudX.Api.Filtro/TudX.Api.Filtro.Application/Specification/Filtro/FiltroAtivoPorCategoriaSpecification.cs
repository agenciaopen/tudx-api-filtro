﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Filtro.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Filtro.Application.Specification
{
    public class FiltroAtivoPorCategoriaSpecification : SpecificationBase<Domain.Model.Filtro>
    {
        private long identificadorCategoria;

        public FiltroAtivoPorCategoriaSpecification(long identificadorCategoria)
        {
            this.identificadorCategoria = identificadorCategoria;
        }

        public override Expression<Func<Domain.Model.Filtro, bool>> SatisfiedBy()
        {
            return f => f.Ativo && f.IdentificadorCategoria == identificadorCategoria;
        }
    }
}
