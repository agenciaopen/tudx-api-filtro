﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Filtro.Domain.Model;
using TudX.Specification;

namespace TudX.Api.Filtro.Application.Specification
{
    public class CaracteristicaPorIdentificadoresSpecification : SpecificationBase<Caracteristica>
    {
        private List<long> identificadores;

        public CaracteristicaPorIdentificadoresSpecification(List<long> identificadores)
        {
            this.identificadores = identificadores;
        }

        public override Expression<Func<Caracteristica, bool>> SatisfiedBy()
        {
            return c => identificadores.Contains(c.Identificador);
        }
    }
}
