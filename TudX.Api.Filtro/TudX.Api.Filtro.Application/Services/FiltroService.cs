﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TudX.Api.Filtro.Application.Specification;
using TudX.Api.Filtro.CrossCuting.Data;
using TudX.Api.Filtro.Domain.Model;
using TudX.Api.Filtro.Domain.Model.Enum;
using TudX.Domain.Base;

namespace TudX.Api.Filtro.Application.Services
{
    public class FiltroService : DomainServiceRelationalBase<Domain.Model.Filtro>
    {
        public FiltroService(IServiceProvider serviceProvider) : base(serviceProvider)
        {            
        }

        public void GerarFiltroDeCaracteristicas(List<Caracteristica> caracteristicas, List<CaracteristicaCategorizadoDataIntegrationEventData> caracteristicasComValor)
        {
            foreach (var caracteristica in caracteristicas)
            {
                Domain.Model.Filtro filtro = 
                    this.FindSingleBySpecification(new FiltroPorIdentificadorDeCaracteristicaSpecification(caracteristica.Identificador), true);

                if (filtro == null)
                {
                    filtro = new Domain.Model.Filtro();
                    filtro.Ativo = true;
                }

                if (filtro.Modificado || !filtro.Ativo)
                    continue;

                filtro.Nome = caracteristica.Nome;
                filtro.IdentificadorCategoria = caracteristica.IdentificadorCategoria;
                filtro.IdentificadorCaracteristica = caracteristica.Identificador;
                TratarOpcoesDoFiltro(filtro, caracteristicasComValor.FirstOrDefault(c => c.IdentificadorCaracteristica == caracteristica.Identificador));

                this.Save(filtro);                
            }
        }

        public List<Domain.Model.Filtro> BuscarFiltrosDaCategoria(long identificadorCategoria)
        {
            FiltroAtivoPorCategoriaSpecification specFiltro = new FiltroAtivoPorCategoriaSpecification(identificadorCategoria);

            return this.FindBySpecification(specFiltro, IncludesOpcoes()).ToList();
        }

        #region Métodos privados
        private void TratarOpcoesDoFiltro(Domain.Model.Filtro filtro, CaracteristicaCategorizadoDataIntegrationEventData caracteristicasComValor)
        {
            if (caracteristicasComValor == null || filtro.Opcoes.Exists(o => o.Valor == caracteristicasComValor.Valor))
                return;

            filtro.Opcoes.Add(new OpcaoFiltro() { Valor = caracteristicasComValor.Valor, Filtro = filtro, TipoElemento = TipoElemento.CheckBox });
        }

        private Expression<Func<Domain.Model.Filtro, object>>[] IncludesOpcoes()
        {
            return new Expression<Func<Domain.Model.Filtro, object>>[] { p => p.Opcoes };
        }
        #endregion
    }
}
