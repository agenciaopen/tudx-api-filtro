﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TudX.Api.Filtro.Application.Specification;
using TudX.Api.Filtro.CrossCuting.Data;
using TudX.Api.Filtro.Domain.Model;
using TudX.Domain.Base;
using TudX.Exceptions;

namespace TudX.Api.Filtro.Application.Services
{
    public class CaracteristicaService : DomainServiceRelationalBase<Caracteristica>
    {
        private readonly CategoriaService categoriaService;
        private readonly FiltroService filtroService;

        public CaracteristicaService(IServiceProvider serviceProvider) : base(serviceProvider)
        {
            categoriaService = new CategoriaService(serviceProvider);
            filtroService = new FiltroService(serviceProvider);
        }

        public void AdicionarCaracteristicaQueFoiCategorizada(CaracteristicaNaoCategorizadaIntegrationEventData caracteristicaNaoCategorizadaIntegrationEventData)
        {
            Caracteristica caracteristica = this.FindById(caracteristicaNaoCategorizadaIntegrationEventData.Identificador);

            if (caracteristica == null)
                caracteristica = new Caracteristica();

            caracteristica.Identificador = caracteristicaNaoCategorizadaIntegrationEventData.Identificador;
            caracteristica.IdentificadorCategoria = caracteristicaNaoCategorizadaIntegrationEventData.IdentificadorCategoria;
            caracteristica.Nome = caracteristicaNaoCategorizadaIntegrationEventData.Nome;

            this.Create(caracteristica);
            this.SaveChanges();
        }

        public void GerarFiltroDasCaracteristicasQueEstaoSendoCriadas(InformacaoCaracteristicaProdutoIntegrationEventData informacaoCaracteristicaProdutoIntegrationEventData)
        {
            Categoria categoria = this.categoriaService.FindById(informacaoCaracteristicaProdutoIntegrationEventData.IdentificadorCategoria);
            if (categoria == null)
                throw new BusinessException("Categoria não encontrada. Diante disto, não será possível adicionar as caracteritiscas, " +
                                            "pois existe uma dependência direta com a categoria.");

            List<Caracteristica> caracteristicas =
                this.FindBySpecification(
                       new CaracteristicaPorIdentificadoresSpecification(
                                    informacaoCaracteristicaProdutoIntegrationEventData.CaracteristicasComValor.Select(c => c.IdentificadorCaracteristica).ToList())).ToList();

            if (caracteristicas == null || caracteristicas.Count <= 0)
                throw new BusinessException("Não existem caracteristicas para gerar os filtros.");

            filtroService.GerarFiltroDeCaracteristicas(caracteristicas, informacaoCaracteristicaProdutoIntegrationEventData.CaracteristicasComValor);
            TratarOpcoesDasCaracteristicasRecebidas(caracteristicas, informacaoCaracteristicaProdutoIntegrationEventData.CaracteristicasComValor);

            this.SaveChanges();
        }

        private void TratarOpcoesDasCaracteristicasRecebidas(List<Caracteristica> caracteristicas, List<CaracteristicaCategorizadoDataIntegrationEventData> caracteristicasComValor)
        {
            foreach (var caracteristica in caracteristicas)
            {
                if (caracteristica.Opcoes == null)
                    caracteristica.Opcoes = new List<OpcaoCaracteristica>();

                CaracteristicaCategorizadoDataIntegrationEventData caracteristicaCategorizadoData =
                    caracteristicasComValor.FirstOrDefault(c => c.IdentificadorCaracteristica == caracteristica.Identificador);

                if (caracteristicaCategorizadoData == null || caracteristica.Opcoes.Exists(c => c.Valor == caracteristicaCategorizadoData.Valor))
                    continue;

                caracteristica.Opcoes.Add(new OpcaoCaracteristica() { Valor = caracteristicaCategorizadoData.Valor, Caracteristica = caracteristica });
                this.Save(caracteristica);
            }
        }
    }
}
