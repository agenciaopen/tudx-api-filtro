﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Filtro.Domain.Model
{
    public class Caracteristica : IEntity
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public long IdentificadorCategoria { get; set; }
        public long? IdentificadorSuperior { get; set; }
        public Caracteristica CaracteristicaSuperior { get; set; }
        public List<Caracteristica> CaracteristicasFilhas { get; set; }
        public List<OpcaoCaracteristica> Opcoes { get; set; }

        public Caracteristica()
        {

        }
    }
}
