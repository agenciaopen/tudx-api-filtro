﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Filtro.Domain.Model
{
    public class OpcaoCaracteristica : IEntity
    {
        public long Identificador { get; set; }
        public string Valor { get; set; }
        public Caracteristica Caracteristica { get; set; }
    }
}
