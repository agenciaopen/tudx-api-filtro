﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Filtro.Domain.Model
{
    public class ValorFiltro : IEntity
    {
        public long Identificador { get; set; }
        public string Valor { get; set; }
        public long IdentificadorCaracteristica { get; set; }
        public OpcaoFiltro OpcaoFiltro { get; set; }        
    }
}
