﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Filtro.Domain.Model
{
    public class Filtro : IEntity
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public string Tutorial { get; set; }
        public bool Ativo { get; set; }
        public bool Modificado { get; set; }
        public long IdentificadorCategoria { get; set; }
        public long? IdentificadorCaracteristica { get; set; }
        public List<OpcaoFiltro> Opcoes { get; set; }

        public Filtro()
        {
            Opcoes = new List<OpcaoFiltro>();
        }
    }
}
