﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Filtro.Domain.Model.Enum;
using TudX.Core.Model;

namespace TudX.Api.Filtro.Domain.Model
{
    public class OpcaoFiltro : IEntity
    {
        public long Identificador { get; set; }
        public string Valor { get; set; }
        public TipoElemento TipoElemento { get; set; }
        public Filtro Filtro { get; set; }
        public long? IdentificadorInformacaoProduto { get; set; }
        public List<ValorFiltro> Valores { get; set; }
    }
}
