﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TudX.Api.Filtro.Domain.Model.Enum
{
    public enum TipoCampo : short
    {
        Nenhum = 0,
        String = 1,
        Short = 2,
        Int = 3,
        DateTime = 4,
        Decimal = 5,
        Double = 6
    }
}
