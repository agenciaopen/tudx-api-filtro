﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Api.Filtro.Domain.Model.Enum;
using TudX.Core.Model;

namespace TudX.Api.Filtro.Domain.Model
{
    public class InformacaoProduto : IEntity
    {
        public long Identificador { get; set; }
        public string NomeCampo { get; set; }
        public TipoCampo TipoCampo { get; set; }
    }
}
