﻿using System;
using System.Collections.Generic;
using System.Text;
using TudX.Core.Model;

namespace TudX.Api.Filtro.Domain.Model
{
    public class Categoria : IEntity
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public long? IdentificadorSuperior { get; set; }
        public Categoria CategoriaSuperior { get; set; }
        public List<Categoria> CategoriasFilhas { get; set; }
    }
}
