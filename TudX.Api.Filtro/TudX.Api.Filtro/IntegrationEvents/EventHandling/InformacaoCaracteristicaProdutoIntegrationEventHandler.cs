﻿using Mapster;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Filtro.Application.Services;
using TudX.Api.Filtro.CrossCuting.Data;
using TudX.Api.Filtro.IntegrationEvents.Events;
using TudX.IntegrationEvents.Application.Service;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Filtro.IntegrationEvents.EventHandling
{
    public class InformacaoCaracteristicaProdutoIntegrationEventHandler : IIntegrationEventHandler<InformacaoCaracteristicaProdutoIntegrationEvent>
    {
        private readonly CaracteristicaService caracteristicaService;
        private readonly EventoRecebidoService eventoRecebidoService;
        private readonly ILogger _logger;

        public InformacaoCaracteristicaProdutoIntegrationEventHandler(IServiceProvider serviceProvider,
                                                        ILoggerFactory loggerFactory)
        {
            caracteristicaService = new CaracteristicaService(serviceProvider);
            eventoRecebidoService = new EventoRecebidoService(serviceProvider);
            _logger = loggerFactory?.CreateLogger<InformacaoCaracteristicaProdutoIntegrationEventHandler>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Handle(InformacaoCaracteristicaProdutoIntegrationEvent @event)
        {
            try
            {
                caracteristicaService.GerarFiltroDasCaracteristicasQueEstaoSendoCriadas(@event.Adapt<InformacaoCaracteristicaProdutoIntegrationEventData>());
                eventoRecebidoService.RegistrarRecebimentoDeEvento(@event);
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Ocorreu um erro ao processar a mensagem: {0} Stack: {1}", ex.Message, ex.StackTrace));
            }
        }
    }
}
