﻿using Mapster;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Filtro.Application.Services;
using TudX.Api.Filtro.CrossCuting.Data;
using TudX.Api.Filtro.IntegrationEvents.Events;
using TudX.IntegrationEvents.Application.Service;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Filtro.IntegrationEvents.EventHandling
{
    public class CargaCategoriaAdicionadaIntegrationEventHandler : IIntegrationEventHandler<CargaCategoriaAdicionadaIntegrationEvent>
    {
        private readonly CategoriaService categoriaService;
        private readonly EventoRecebidoService eventoRecebidoService;
        private readonly ILogger _logger;

        public CargaCategoriaAdicionadaIntegrationEventHandler(IServiceProvider serviceProvider,
                                                               ILoggerFactory loggerFactory)
        {
            categoriaService = new CategoriaService(serviceProvider);
            eventoRecebidoService = new EventoRecebidoService(serviceProvider);
            _logger = loggerFactory?.CreateLogger<CargaCategoriaAdicionadaIntegrationEventHandler>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Handle(CargaCategoriaAdicionadaIntegrationEvent @event)
        {
            try
            {
                categoriaService.InserirCargaCategoria(@event.Adapt<CargaCategoriaIntegrationEventData>());
                eventoRecebidoService.RegistrarRecebimentoDeEvento(@event);
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format("Ocorreu um erro ao processar a mensagem: {0} Stack: {1}", ex.Message, ex.StackTrace));
            }
        }
    }
}
