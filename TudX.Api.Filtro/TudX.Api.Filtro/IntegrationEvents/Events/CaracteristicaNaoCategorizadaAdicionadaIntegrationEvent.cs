﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.IntegrationEvents.Events;

namespace TudX.Api.Filtro.IntegrationEvents.Events
{
    public class CaracteristicaNaoCategorizadaAdicionadaIntegrationEvent : IntegrationEvent
    {
        public long Identificador { get; set; }
        public string Nome { get; set; }
        public long IdentificadorCategoria { get; set; }
    }
}
