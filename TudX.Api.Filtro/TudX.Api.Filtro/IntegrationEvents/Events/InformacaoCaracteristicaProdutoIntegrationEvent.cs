﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Filtro.IntegrationEvents.Events.Data;
using TudX.IntegrationEvents.Events;

namespace TudX.Api.Filtro.IntegrationEvents.Events
{
    public class InformacaoCaracteristicaProdutoIntegrationEvent : IntegrationEvent
    {
        public long IdentificadorCategoria { get; set; }
        public List<CaracteristicaCategorizadoDataIntegrationEvent> CaracteristicasComValor { get; set; }
    }
}
