﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TudX.Api.Base.Controller;
using TudX.Api.Filtro.Application.Services;
using TudX.Api.Filtro.CrossCuting.Data;
using TudX.Exceptions;

namespace TudX.Api.Filtro.Controllers
{

    /// <summary>
    /// Fornece funcionalidades referentes a entidade CategoriaLoja.
    /// </summary>
    [ApiVersion("1.0")]
    public class FiltroController : TudxApiController
    {
        private readonly FiltroService filtroService;

        public FiltroController(IServiceProvider serviceProvider)
        {
            filtroService = new FiltroService(serviceProvider);
        }

        /// <summary>
        /// Busca os filtros possíveis de uma categoria.
        /// </summary>
        /// <response code="200">Dados da categoria para carga</response>
        /// <response code="400">Dados de erro de negócio</response>
        /// <response code="500">Dados de erro de framework</response>
        /// <param name="categoriaData">Dados da categoria.</param>
        /// <returns>Dados da loja inserida.</returns>
        [HttpGet("{identificadorCategoria}")]
        [ProducesResponseType(typeof(List<FiltroData>), 200)]
        [ProducesResponseType(typeof(BusinessException), 400)]
        [ProducesResponseType(typeof(Exception), 500)]
        public async Task<IActionResult> BuscarFiltro(long identificadorCategoria)
        {
            List<Domain.Model.Filtro> filtros = filtroService.BuscarFiltrosDaCategoria(identificadorCategoria);            

            return Ok(filtros.Adapt<List<FiltroData>>());
        }
    }
}