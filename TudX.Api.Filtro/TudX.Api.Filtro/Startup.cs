﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TudX.Adapter.Eventbus.RabbitMQ.Extensions;
using TudX.Api.Base;
using TudX.Api.Base.Metadata;
using TudX.Api.Filtro.Infra.EF.DependencyInjection;
using TudX.Api.Filtro.IntegrationEvents.EventHandling;
using TudX.Api.Filtro.IntegrationEvents.Events;
using TudX.IntegrationEvents.EventBus.Abstractions;

namespace TudX.Api.Filtro
{
    /// <summary>
    /// Configuração de inicio da aplicação.
    /// </summary>
    public class Startup : TudxApiBaseStartup
    {
        /// <summary>
        /// Informações a serem apresentadas na documentação do swagger.
        /// </summary>
        protected override ApiMetadata ApiMetadata => new ApiMetadata()
        {
            Name = "Api Filtro",
            Description = "Esta api será utilizada para gerar as opções de filtros das várias categorias.",
            DefaultApiVersion = "1.0"
        };

        /// <summary>
        /// Construtor.
        /// </summary>
        /// <param name="configuration">Informações de configurações da aplicação.</param>
        public Startup(IConfiguration configuration) : base(configuration)
        {

        }

        /// <summary>
        /// Métodos de configurações de IoC
        /// </summary>
        /// <param name="services">Classe responsável por configurar o DI.</param>
        protected override void ConfigureApiServices(IServiceCollection services)
        {
            services.AddControllers().AddNewtonsoftJson();
            services.AddCors(options =>
            {
                options.AddPolicy("TudXPolicy",
                builder =>
                {
                    builder.WithOrigins("http://tudx01",
                                        "http://localhost",
                                        "http://localhost:4200")
                                        .AllowAnyHeader()
                                        .AllowAnyMethod()
                                        .AllowCredentials();
                });
            });

            services.AddEFAdapter(Configuration);
            services.AddRabbitMQAdapter(Configuration);

            services.AddTransient<CaracteristicaNaoCategorizadaAdicionadaIntegrationEventHandler>();
            services.AddTransient<CargaCategoriaAdicionadaIntegrationEventHandler>();
            services.AddTransient<CargaCategoriaAlteradaIntegrationEventHandler>();
            services.AddTransient<CategoriaAdicionadaIntegrationEventHandler>();
            services.AddTransient<CategoriaAlteradaIntegrationEventHandler>();
            services.AddTransient<InformacaoCaracteristicaProdutoIntegrationEventHandler>();
        }


        /// <summary>
        /// Método de configuração da aplicação.
        /// </summary>
        /// <param name="app">Construtor da aplicação.</param>
        protected override void ConfigureApi(IApplicationBuilder app)
        {
            var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

            eventBus.Subscribe<CaracteristicaNaoCategorizadaAdicionadaIntegrationEvent, CaracteristicaNaoCategorizadaAdicionadaIntegrationEventHandler>();
            eventBus.Subscribe<CargaCategoriaAdicionadaIntegrationEvent, CargaCategoriaAdicionadaIntegrationEventHandler>();
            eventBus.Subscribe<CargaCategoriaAlteradaIntegrationEvent, CargaCategoriaAlteradaIntegrationEventHandler>();
            eventBus.Subscribe<CategoriaAdicionadaIntegrationEvent, CategoriaAdicionadaIntegrationEventHandler>();
            eventBus.Subscribe<CategoriaAlteradaIntegrationEvent, CategoriaAlteradaIntegrationEventHandler>();
            eventBus.Subscribe<InformacaoCaracteristicaProdutoIntegrationEvent, InformacaoCaracteristicaProdutoIntegrationEventHandler>();

            app.UseCors("TudXPolicy");
            app.UseRouting();
            app.UseEndpoints(endpoints => {
                endpoints.MapControllers();
            });
        }
    }
}
