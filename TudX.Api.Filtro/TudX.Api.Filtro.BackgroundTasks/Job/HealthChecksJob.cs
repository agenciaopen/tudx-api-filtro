﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TudX.Api.Filtro.Infra.Refit.Clients;

namespace TudX.Api.Filtro.BackgroundTasks.Job
{
    public class HealthChecksJob : IJob
    {
        private readonly ILogger<HealthChecksJob> _logger;
        private readonly IServiceProvider serviceProvider;

        public HealthChecksJob(IServiceProvider serviceProvider, ILogger<HealthChecksJob> logger)
        {
            _logger = logger;
            this.serviceProvider = serviceProvider;
        }

        public Task Execute(IJobExecutionContext context)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                // Resolve the Scoped service
                var apiFiltro = scope.ServiceProvider.GetService<IApiFiltro>();
                string teste = apiFiltro.HealthChecksAsync().Result;
                _logger.LogInformation(teste);
            }

            return Task.CompletedTask;
        }
    }
}
